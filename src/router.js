import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import Home from './views/Home.vue';

Vue.use(Router);

const requireAuth = (to, from, next) => (store.state.userIsAuthenticated ? next() : next({ name: 'login', query: { redirect: to.path } }));

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/signup',
      name: 'signup',
      // route level code-splitting
      // this generates a separate chunk (signup.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "signup" */ './views/Signup.vue'),
      props: (route) => ({ redirect: route.query.redirect }),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue'),
      props: (route) => ({ redirect: route.query.redirect })
    },
    {
      path: '/notes/new',
      name: 'new-note',
      component: () => import(/* webpackChunkName: "new-note" */ './views/NewNote.vue'),
      beforeEnter: requireAuth,
    },
    {
      path: '/notes/:id',
      name: 'note-details',
      component: () => import(/* webpackChunkName: "note-details" */ './views/NoteDetails.vue'),
      beforeEnter: requireAuth,
    },
    {
      path: '*',
      name: '404',
      component: () => import(/* webpackChunkName: "404" */ './views/404.vue'),
    },
  ],
});
