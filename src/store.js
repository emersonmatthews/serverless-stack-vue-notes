import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userIsAuthenticated: false,
  },
  mutations: {
    LOG_IN(state) {
      state.userIsAuthenticated = true;
    },
    LOG_OUT(state) {
      state.userIsAuthenticated = false;
    },
  },
  actions: {

  },
});
