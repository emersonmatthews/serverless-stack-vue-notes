export default {
  cognito: {
    REGION: 'us-east-1',
    USER_POOL_ID: 'us-east-1_nBh6Madaa',
    APP_CLIENT_ID: '6jfqqfkjsmp4q0lq6pvhpm8v4u',
    IDENTITY_POOL_ID: 'us-east-1:5e7c579f-2de2-4ffc-a5e0-40a252af8561',
  },
  s3: {
    REGION: 'us-east-1',
    BUCKET: 'notes-app-api-dev-attachmentsbucket-1m5o2xy1le0zd',
  },
  apiGateway: {
    REGION: 'us-east-1',
    URL: 'https://240irm0mm5.execute-api.us-east-1.amazonaws.com/dev',
  },
  MAX_ATTACHMENT_SIZE: 5000000, // 5MB
}
